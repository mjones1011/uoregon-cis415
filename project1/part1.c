#include "part1.h"

/*
Mason Jones 951721926 -- CIS 415 Project 1
This is all my own work except for trimTrailing.
*/

//********************
//Functions
//********************

//SOURCE
//https://codeforwin.org/2016/04/c-program-to-trim-trailing-white-space-characters-in-string.html
//Trims trailing white space, tabs, and newlines
void trimTrailing(char *str){
	int index = -1;
	int i = 0;
	while(str[i] != '\0'){
		if(str[i] != ' ' && str[i] != '\t' && str[i] != '\n'){
			index = i;
		}
		i++;
	}
	str[index + 1] = '\0';
}

//Counts # args in PCB
int argCount(char *line){
	int count = 0;
	char *dupe = strdup(line);
	char *token = strtok(dupe, " \t\n");
	count++;
	while((token = strtok(NULL, " \t\n")) != NULL){
		count++;
	}
	free(token);
	free(dupe);
	return count;
}

//Creates a node that is filled with the data in a given line
struct PCB* mallocNode(char *line){
	//strdup
	char *line_dupe = strdup(line);								
	//strdup for argCount
	char *dupe2 = strdup(line);
	//# args including CMD
	int argAmt = argCount(dupe2);
	//strtok token
	char *token;
	//args counter
	int i = 0;
	
	struct PCB *node = malloc (sizeof(struct PCB));
	
	node->next = NULL;
	
	//allocate for amt of args
	node->args = (char **) malloc ((argAmt + 1) * (sizeof(char *)));

	//get first cmd
	token = strtok(line_dupe, " \t\n");

	//assign cmd to CMD and args[0]
	node->CMD = strdup(token);
	node->args[i] = strdup(token);
	i++;

	//assign rest of args
	while(i < argAmt){
		token = strtok(NULL, " \t\n");
		node->args[i] = strdup(token);
		i++;
	}
	//last arg == NULL
	node->args[i] = NULL;
	
	int PID = -1;
	
	//free
	free(dupe2);
	free(line_dupe);
	return node;
}

//Counts # PCBs in linked list
int countProcessControlBlocks(struct PCB *head){
	struct PCB *node = head;
	int count = 0;
	while(node->next != NULL){
		count++;
		node = node->next;
	}
	if(node->next == NULL){
		if(node->CMD != NULL){
			count++;
		}
	}
	return count;
}

//Prints each PCB's info (early testing function)
void printPCBs(struct PCB *head){
	struct PCB *node = head;
	int amt = countProcessControlBlocks(node);
	int i; //counter
	int x = 0; //PCB counter
	if(node != NULL){
		printf("\nPrinting PCBs:\n");
		while(x < amt){
			printf("CMD: %s\n", node->CMD);
			if(node->args[0] != NULL){
				i = 0;
				while(node->args[i] != NULL){
					printf("Arg %d: %s\n", i, node->args[i]);
					i++;
				}
			}
			node = node->next;
			x++;
		}
	}
}

//Main function that takes the file and creates all necessary PCBs
struct PCB* makeProcessControlBlocks(FILE *file){
	struct PCB *head;
	struct PCB *placeholder;
	char *line = NULL;
	ssize_t read = 0;
	size_t len = 0;

	//get first line from file
	//fill 'head'
	read = getline(&line, &len, file);

	trimTrailing(line);										
	head = mallocNode(line);									

	//set placeholder to first node
	placeholder = head;

	//fill rest of nodes
	//while more lines in file
	while((read = getline(&line, &len, file)) != -1){
		
		trimTrailing(line);	

		//make new node with info
		struct PCB *node = mallocNode(line);					

		//find where to put node
		while(head->next != NULL){
			head = head->next;
		}

		//place node
		head->next = node;
	}

	//set head back to first node
	head = placeholder;
	
	//free
	free(line);
	return head;
}

//Launches all PCBs
int launchAll(struct PCB *head){
	struct PCB *node = head;
	int countPCB = countProcessControlBlocks(node);
	int i = 0;
	int status = 0; //3 = fail

	printf("\nStarting launch\n");
	while(i < countPCB){
		node->PID = fork();
		//printf("PID: %d\n", node->PID);
		if(node->PID < 0){
			perror("damn: ");
			errno = 0;
			status = 3;
			break;
		}
		if(node->PID == 0){
			execvp(node->CMD, node->args);
			freeNodes(&head);
			perror("ERROR! Starting program failed!\n");
			errno = 0;
			puts("Exiting so duplicate copy of MPC doesn't run/fork bomb.\n");
			exit(-1);
		}
		node = node->next;
		i++;
	}
	//if pid < 0 burn it down
	i = 0;
	if(status == 3){
		node = head;
		while(i < countPCB){
			kill(node->PID, 9);
			node = node->next;
		}
	}
	return status;
}

//Waits for all processes to finish running
void waitForAllExit(struct PCB *head){
	struct PCB *node = head;
	int countPCB = countProcessControlBlocks(node);
	int i = 0;

	printf("\nWaiting for exit\n");
	while(i < countPCB){
		pid_t status = waitpid(node->PID, NULL, WNOHANG);
		if(status == -1){
			perror("ERROR IN WAITPID!\n");
			errno = 0;
		}
		if(status == 0){
			while(status == 0){
				status = waitpid(node->PID, NULL, WNOHANG);
			}
		}
		node = node->next;
		i++;
	}
}

//Frees all allocated memory
void freeNodes(struct PCB **head){
	struct PCB *tmp;
	int i = 0;
	int j = 0;
	int amt = countProcessControlBlocks(*head);
	while(j < amt){
		tmp = *head;
		*head = tmp->next;
		while(tmp->args[i] != NULL){
			//printf("free: %s\n", tmp->args[i]);
			free(tmp->args[i]);
			i++;
		}
		free(tmp->args);
		free(tmp->CMD);
		free(tmp);
		j++;
		i = 0;
	}
}

//********************
//Main
//********************
int main(int argc, char *argv[]){
	if(argc >= 2){
		struct PCB *node = NULL;
		FILE *in = NULL;
		int fileOK = 0;		//ok == 0, not ok == -1

		//get file in
		char *fileIn = argv[1];
		//if this is a valid file
		if(in = fopen(fileIn, "r")){
			puts("File Valid!\n");
		}
		else{
			puts("File INVALID! Shutting down.\n");
			fileOK = -1;
		}
		if(fileOK == 0){
			//make PCBs
			node = makeProcessControlBlocks(in);
			fclose(in);
			//print info
			//printPCBs(node);
			//launch all, returns 3 if processes burned down
			int status = launchAll(node);
			//if processes are ok, wait for them
			if(status == 0){
				waitForAllExit(node);
			}
			//free processes
			freeNodes(&node);
		}
	}
	return 0;
}