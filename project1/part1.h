#ifndef PART1_H
#define PART1_H
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <signal.h>
#include <errno.h>

struct PCB{
	//Linked list
	struct PCB *next;

	char *CMD;
	char **args;
	int PID;
	
	//Status state;
	//int hasExited;
	
	//int userTime;
	//int sysTime;

	//int timeToSchedule;
};

//Part 2
void SIGUSR1_FUNCT(int signal);

void sendAllProgramsSignal(struct PCB *head, int signal);

//Part 1
void trimTrailing(char *str);

int argCount(char *line);

struct PCB* mallocNode(char *line);

int countProcessControlBlocks(struct PCB *head);

void printPCBs(struct PCB *head);

struct PCB* makeProcessControlBlocks(FILE *file);

int launchAll(struct PCB *head);

void waitForAllExit(struct PCB *head);

void freeNodes(struct PCB **head);

#endif