#define _GNU_SOURCE
#include "anagram.h"

/**
Mason Jones 951721926 -- CIS 415 Project 0
This file is my own work.
**/

int main(int argc, char *argv[]){
	//File I/O pointers
	FILE *in = NULL;
	FILE *out = NULL;

	//Variables for 'while' loop
	char *line = NULL;
	ssize_t read = 0;
	size_t len = 0;

	//NULL initial pointer
	struct AnagramList *mainList = NULL;
	
	//Names of I/O files
	char *fileIn = argv[1];
	char *fileOut = argv[2];

	//Get input
	if(fileIn != NULL){
		in = fopen(fileIn, "r");
	}
	else{
		in = stdin;
	}

	//Get output
	if(fileOut != NULL){
		out = fopen(fileOut, "a");
	}
	else{
		out = stdout;
	}

	//Read in line by line from 'in' file
	while((read = getline(&line, &len, in)) != -1){
		line[strcspn(line, "\n")] = '\0';
		AddWordAList(&mainList, line);
	}

	//Print out results
	PrintAList(out, mainList);

	//Close I/O
	fclose(in);
	fclose(out);

	//Free stuff
	FreeAList(&mainList);
	free(line);

	return 0;
}
