#include "anagram.h"

/**
Mason Jones 951721926 -- CIS 415 Project 0
This is my own work except for compareFunction and quickSort.
**/

//********************
//Added Functions
//********************
void toLowerCase(char *word){
	int i;
	for(i = 0; word[i] != '\0'; i++){
		word[i] = tolower(word[i]);
	}
}

//SOURCE
//https://stackoverflow.com/questions/23147569/using-qsort-for-character-array-in-c
int compareFunction(const void *a, const void *b){
	return *(char*)a - *(char*)b;
}

//SOURCE
//https://stackoverflow.com/questions/23147569/using-qsort-for-character-array-in-c
void quickSort(char *word){
	qsort(word, (size_t)strlen(word), (size_t)sizeof(char), compareFunction);
}

int countStringNodes(struct StringList **head){
	struct StringList *tmp = *head;
	int i = 1;
	for(; tmp->Next != NULL; tmp = tmp->Next){
		i++;
	}
	return i;
}

int countAnagramNodes(struct AnagramList **head){
	struct AnagramList *tmp = *head;
	int i = 0;
	for(; tmp->Next != NULL; tmp = tmp->Next){
		i++;
	}
	return i;
}

//********************
//StringList Functions
//********************
struct StringList *MallocSList(char *word){
	//Allocate memory for this node
	struct StringList *theList = malloc (sizeof(struct StringList));
	
	//Set the  "Next" link to NULL
	theList->Next = NULL;

	theList->Word = NULL;
	//Set the word
	theList->Word = strdup(word);

	return theList;
}

void AppendSList(struct StringList **head, struct StringList *node){
	struct StringList *tmp = *head;
	for(; tmp->Next != NULL; tmp = tmp->Next);
	tmp->Next = node;
}

void FreeSList(struct StringList **node){
	struct StringList *tmp;
	while(*node != NULL){
		tmp = *node;
		*node = tmp->Next;
		free(tmp->Word);
		free(tmp);
	}
}
	
void PrintSList(FILE *file, struct StringList *node){
	if(file != NULL){
		//For all nodes but last
		for(; node->Next != NULL; node = node->Next){
			fprintf(file, "\t%s\n", node->Word);
		}
		//For last node
		if(node->Next == NULL){
			fprintf(file, "\t%s\n", node->Word);
		}
	}
	else{
		printf("Error! Could not open file!");
	}
}

//********************
//AnagramList Functions
//********************
struct AnagramList* MallocAList(char *word){
	//Allocate memory for this AnagramList node
	struct AnagramList *node = malloc (sizeof(struct AnagramList));

	//Set "Next" link to NULL
	node->Next = NULL;

	//Clone 'word' for StringList
	char *clone = strdup(word);

	//Change to SORTED lower case
	toLowerCase(word);
	quickSort(word);
	
	//Set Anagram to word
	node->Anagram = strdup(word);

	//Start a StringList that contains words related to this anagram
	node->Words = MallocSList(clone);
	
	//Free memory of clone
	free(clone);
	return node;
}

void FreeAList(struct AnagramList **node){
	struct AnagramList *tmp;
	while(*node != NULL){
		//printf("ALIST POS %d: FREED!\n", i);
		tmp = *node;
		*node = tmp->Next;
		FreeSList(&tmp->Words);
		free(tmp->Anagram);
		free(tmp);
	}
}

void PrintAList(FILE *file,struct AnagramList *node){
	struct AnagramList *tmp = node;
	if(file != NULL){
		//For all nodes but last
		for(; tmp->Next != NULL; tmp = tmp->Next){
			int num = countStringNodes(&tmp->Words);
				if(num > 1){
				fprintf(file, "%s:%d\n", tmp->Anagram, num);
				PrintSList(file, tmp->Words);
			}
		}
		//For last node
		if(tmp->Next == NULL){
			int num = countStringNodes(&tmp->Words);
			if(num > 1){
				fprintf(file, "%s:%d\n", tmp->Anagram, num);
				PrintSList(file, tmp->Words);
			}
		}
	}
	else{	
		printf("Error! Could not find file!\n");
	}
}

void AddWordAList(struct AnagramList **node, char *word){
	struct AnagramList *tmp = *node;
	struct AnagramList *tmp2 = *node;
	//Possible memory leak for if()?
	if(*node == NULL){
		*node = MallocAList(word);
		//printf("NULL AnagramList node given: %s\n", (*node)->Anagram);
	}
	else{
		//Clone 'word' to preserve original's capitalization/order
		char *clone = strdup(word);

		//Sort and send to lower case in order to compare
		toLowerCase(clone);
		quickSort(clone);

		int found = 0;
		
		//Searching through AnagramList
		for(; found == 0 && tmp->Next != NULL; tmp = tmp->Next){
			//printf("Searching: %s...\n", tmp->Anagram);
				
			//If there is a matching Anagram
			if(strcmp(tmp->Anagram, clone) == 0){

				//Create new StringList node
				struct StringList *newNode = MallocSList(word);

				//Append word to StringList
				AppendSList(&tmp->Words, newNode);
				//printf("%s appended to '%s' StringList!\n", word, tmp->Anagram);
				
				//Found a match, break loop
				found = 1;
			}
		}

		//Check last node (node->Next == NULL but contains Anagram)
		if(found == 0){
			//printf("Searching: %s...\n", tmp->Anagram);
				
			//If there is a matching Anagram
			if(strcmp(tmp->Anagram, clone) == 0){

				//Create new StringList node
				struct StringList *newNode = MallocSList(word);

				//Append word to StringList
				AppendSList(&tmp->Words, newNode);
				//printf("%s appended to '%s' StringList!\n", word, tmp->Anagram);
				found = 1;
			}
		}

		//If no matching Anagram
		if(found == 0){
			struct AnagramList *newNode = MallocAList(word);
			if(tmp2->Next != NULL){
				for(; tmp2->Next != NULL; tmp2 = tmp2->Next);
			}
			tmp2->Next = newNode;
			//printf("Add new AnagramList node: %s\n", newNode->Anagram);
		}
		free(clone);
	}
}