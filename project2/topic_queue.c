#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <string.h>
#include <unistd.h>
#include "thread_safe_bounded_queue.h"
#include "topic_queue.h"

/*
Mason Jones 951721926 -- CIS 415 Project 2
This is all my own work.
*/

//********************
//STRUCTS
//********************
struct topic{
    int entryNum;
    struct timeval timestamp;
    int pubID;
    char *photoURL; //QUACKSIZE
    char *photoCaption; //CAPTIONSIZE
};

struct queue_topics{
	int numEntries;
	TSBoundedQueue *queue;
	char *name;
	int id;
};

//********************
//FUNCTIONS
//********************
TopicEntry *MallocTopicEntry(char *url, char *caption, int entryNum){
        struct topic *returnValue = malloc (sizeof(struct topic));
        
        //int url_len = strlen(url);
        //int cap_len = strlen(caption);

        returnValue->photoURL = strdup(url);
        returnValue->photoCaption = strdup(caption);

        //returnValue->photoURL = (char *) malloc (url_len * sizeof(char));
        //returnValue->photoCaption = (char *) malloc (cap_len * sizeof(char));

        //returnValue->photoURL = url;
        //returnValue->photoCaption = caption;


        returnValue->entryNum = entryNum;
        //returnValue->timestamp = NULL;
        returnValue->pubID = 0;

        return returnValue;
}

TopicQueue *MallocTopicQueue(char *name, int id, int MAXENTRIES){
	struct queue_topics *topicqueue = malloc (sizeof(struct queue_topics));

	topicqueue->queue = TS_BB_MallocBoundedQueue(MAXENTRIES);
	topicqueue->numEntries = 0;
	topicqueue->name = strdup(name);
	topicqueue->id = id;

	return topicqueue;
}

//Returns head id if OK, -1 if queue full
long long enqueueTopic(struct queue_topics *topicqueue, char *url, char *caption){
	long long returnValue = -1;
	void *item = NULL;
	TopicEntry *topic = (TopicEntry *)item;
	if(TS_BB_IsFull(topicqueue->queue) == 0){
		
		//Create topic entry
		topic = MallocTopicEntry(url, caption, topicqueue->numEntries+1);
		//Set topic's entrynum to numEntries + 1
		topic->entryNum = topicqueue->numEntries + 1;
		//Timestamp topic entry
		gettimeofday(&(topic->timestamp), NULL);

		TopicEntry *entry = topic;
		
		//Try to enqueue topic entry
		if(TS_BB_TryEnqueue(topicqueue->queue, entry) > -1){
			//Increment numEntries
			topicqueue->numEntries++;
			//Return value == head id
			returnValue = TS_BB_GetFront(topicqueue->queue);

			printf("Topic entry %s enqueued with entry #%d!\n", topic->photoURL, entry->entryNum);
		}
	}
	else{
		puts("Topic Queue is full!\n");
	}
	return returnValue;
}

int dequeueTopics(struct queue_topics *topicqueue, long DELTA){
	int returnValue = 0;
	long long i;
	//For all topicentries
	for(i = TS_BB_GetBack(topicqueue->queue); i <= TS_BB_GetFront(topicqueue->queue); i++){
		//Get topicentry
		TopicEntry *t = (TopicEntry *)TS_BB_GetItem(topicqueue->queue, i);
		//Get timestamp
		struct timeval timestamp = t->timestamp;
		struct timeval temp;
		gettimeofday(&temp, NULL);
		//Convert new time to float
		time_t newtime = temp.tv_sec;

		//if newtime-oldtime >= DELTA (in seconds)
		if((long)(newtime-timestamp.tv_sec) >= DELTA){
			//WE DO GET HERE
			if(TS_BB_TryDequeue(topicqueue->queue, t->entryNum-1) == 1){
				puts("Successful dequeue!\n");
				returnValue = 1;
			}
		}
	}
	return returnValue;
}

long long getEntry(struct queue_topics *topicqueue, struct topic **t, long long lastentry){
	long long returnValue = -1;
	//If empty (CASE 1)
	if(TS_BB_IsEmpty(topicqueue->queue) == 1){
		returnValue = 0;
	}
	else{
		//If lastentry+1 in queue (CASE 2)
		if(TS_BB_IsIdValid(topicqueue->queue, (lastentry+1)) == 1){
			*t = (TopicEntry *)TS_BB_GetItem(topicqueue->queue, lastentry+1);
			returnValue = 1;
		}
		//Queue not empty, lastentry+1 not found (CASE 3)
		else{
			//(3a)
			if(TS_BB_GetFront(topicqueue->queue) < lastentry){
				printf("Entry number %lld has not been placed in the queue yet!\n", lastentry);
				returnValue = 0;
			}
			//(3b)
			if(TS_BB_GetFront(topicqueue->queue) > lastentry){
				long long tail = TS_BB_GetBack(topicqueue->queue);
				long long head = TS_BB_GetFront(topicqueue->queue);
				//Increment through entries in queue until you find which entry is closest to lastentry
				while(tail < head){
					if(tail > lastentry){
						*t = (TopicEntry *)TS_BB_GetItem(topicqueue->queue, tail);
						returnValue = (*t)->entryNum;
						break;
					}
					tail++;
				}
			}
		}
	}
	return returnValue;
}

//********************
//Main
//********************
int main(int argc, char *argv[]){
	//Create Queue
	char name[] = "Sports";
	struct queue_topics *tqueue = MallocTopicQueue(name, 1, 5);
	//Get lastentry (0)
	long long lastentry = TS_BB_GetFront(tqueue->queue);
	int getEntryResult = -1;

	//Fill queue
	enqueueTopic(tqueue, "www.reddit.com", "This is funny!");
	enqueueTopic(tqueue, "www.google.com", "This is a search engine!");
	enqueueTopic(tqueue, "www.twitter.com", "This is the best social media platform!");
	enqueueTopic(tqueue, "www.shoes.com", "This is the site where you got my shoes from!");
	enqueueTopic(tqueue, "www.albertsons.com", "This is where you buy your groceries!");
	//Extra enqueue should fail
	enqueueTopic(tqueue, "www.amazon.com", "This is where you buy stuff!");

	//Check count
	printf("%d entries enqueued!\n", TS_BB_GetCount(tqueue->queue));

	//Get each entry and keep track of lastentry
	int i = 0;
	TopicEntry *t = NULL;
	while(i < TS_BB_GetCount(tqueue->queue)){
		getEntryResult = getEntry(tqueue, &t, lastentry);
			if(getEntryResult > 0){
			lastentry = t->entryNum-1;
			printf("Last entry that was read: %lld\n", lastentry+1);
			printf("%s -- %s\n", t->photoURL, t->photoCaption);
			i++;
		}
	}

	//Extra enqueue should fail
	enqueueTopic(tqueue, "www.amazon.com", "This is where you buy stuff!");
	
	//Empty the queue
	sleep(5);
	puts("Dequeueing topics...\n");
	dequeueTopics(tqueue, 4);

	//Make sure queue is empty
	if(TS_BB_IsEmpty(tqueue->queue) == 1){
		puts("Topic queue is now empty!\n");
	}

	//Re-fill queue
	enqueueTopic(tqueue, "www.amazon.com", "This is where you buy stuff!");
	enqueueTopic(tqueue, "www.facebook.com", "This is cancer!");
	sleep(2);
	enqueueTopic(tqueue, "www.chewy.com", "This is where you buy cat food!");
	enqueueTopic(tqueue, "www.pcpartpicker.com", "This is where you build a computer!");
	enqueueTopic(tqueue, "www.lowes.com", "This is where you buy appliances!");

	//Delete some things
	puts("Dequeueing topics older than 2 seconds...\n");
	dequeueTopics(tqueue, 2);

	//Read "next entry" (Should return tail of queue since lastentry not valid)
	printf("lastentry: %lld\n", lastentry+1);
	getEntryResult = getEntry(tqueue, &t, lastentry);
	if(getEntryResult > 0){
		lastentry = t->entryNum-1;
		printf("Last entry that was read: %lld\n", lastentry+1);
		printf("%s -- %s\n", t->photoURL, t->photoCaption);
	}

	//Read "next entry"
	getEntryResult = getEntry(tqueue, &t, lastentry);
	if(getEntryResult > 0){
		lastentry = t->entryNum-1;
		printf("Last entry that was read: %lld\n", lastentry+1);
		printf("%s -- %s\n", t->photoURL, t->photoCaption);
	}

	//Read "next entry" (Should return tail of queue)
	getEntryResult = getEntry(tqueue, &t, 200);
	//Should fail and never enter this 'if' statement
	if(getEntryResult > 0){
		lastentry = t->entryNum-1;
		printf("Last entry that was read: %lld\n", lastentry+1);
		printf("%s -- %s\n", t->photoURL, t->photoCaption);
	}
	TS_BB_FreeBoundedQueue(tqueue->queue);
	free(t);
	free(tqueue);

	return 0;
}