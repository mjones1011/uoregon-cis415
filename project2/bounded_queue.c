#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include "bounded_queue.h"

/*
Mason Jones 951721926 -- CIS 415 Project 2
This is all my own work.
*/

//********************
//STRUCTS
//********************
struct bounded_queue{
        int size;       // capacity
        void **buffer;  // storage
        long long head; // 
        long long tail; //
};

//********************
//FUNCTIONS
//********************
int RoundIDToBufferIndex(int size, long long index){
        long long value = (index % ((long long)size));
        return (int)value;
}

BoundedQueue *BB_MallocBoundedQueue(long size){
        //Create BoundedQueue and malloc
        struct bounded_queue *returnValue = (struct bounded_queue *) malloc (sizeof(struct bounded_queue));
        
        //malloc (size) amount of entries
        returnValue->buffer = (void **) malloc (size * sizeof(void *));

        //Initialize values
        returnValue->size = size;
        returnValue->head = 0;
        returnValue->tail = 0;

        return (BoundedQueue *)returnValue;
}

//returns id if pass, -1 if fail
long long BB_TryEnqueue(struct bounded_queue *queue,void *item){
        long long returnValue = -1;

        //if queue not full
        if(BB_IsFull(queue) != 1){
                //get head id
                int index = RoundIDToBufferIndex(queue->size, queue->head);
                queue->buffer[index] = item;

                //increment head
                queue->head++;

                //set OK
                returnValue = queue->head - 1;
        }
        return returnValue;
}

//1 if pass, 0 if fail
int BB_TryDequeue(struct bounded_queue *queue,long long id){
        int returnValue = 0;
        if(BB_IsEmpty(queue) != 1 && BB_IsIdValid(queue, id) == 1){
                if(queue->tail == id){
                        int index = RoundIDToBufferIndex(queue->size, queue->tail); 
                        queue->buffer[index] = NULL;
                        queue->tail++;
                        returnValue++;
                }
        }
        return returnValue;
}

//Returns id of head - 1, -1 if empty
long long BB_GetFront(struct bounded_queue *queue){
        long long returnValue = -1;
        if(BB_IsEmpty(queue) != 1){
                returnValue = queue->head - 1;
        }
        return returnValue;
}

//Returns id of tail, -1 if empty
long long BB_GetBack(struct bounded_queue *queue){
        long long returnValue = -1;
        if(BB_IsEmpty(queue) != 1){
                returnValue = queue->tail;
        }
        return returnValue;
}

//Returns headID - tailID
int BB_GetCount(struct bounded_queue *queue){
        long long returnValue = 0;
        returnValue = queue->head - queue->tail;
        return (int)returnValue;
}

//1 if valid, 0 if invalid
int BB_IsIdValid(struct bounded_queue *queue,long long id){
        int returnValue = 0;
        if(queue->head > id && queue->tail <= id){
                returnValue++;
        }
        return returnValue;
}

//Returns a TopicEntry with the matching ID
void *BB_GetItem(struct bounded_queue *queue,long long id){
        void *returnValue = NULL;
        int empty = BB_IsEmpty(queue);

        //If queue empty
        if(empty == 1){
                puts("BB_GetItem: BoundedQueue empty!\n");
        }
        else{
                int result = BB_IsIdValid(queue, id);
                //If ID valid
                if(result == 1){
                        int index = RoundIDToBufferIndex(queue->size, id);
                        returnValue = queue->buffer[index];
                }
        }
        return returnValue;
}

//1 if full, 0 if empty
int BB_IsFull(struct bounded_queue *queue){
        int returnValue = 0;
        if((queue->head) - (queue->tail) == queue->size){
                returnValue = 1;
        }
        return returnValue;
}
//1 if empty, 0 if not empty
int BB_IsEmpty(struct bounded_queue *queue){
        int returnValue = 0;
        if(queue->head == queue->tail){
                returnValue = 1;
        }
        return returnValue;
}

void BB_FreeBoundedQueue(struct bounded_queue *queue){
        free(queue->buffer);
        free(queue);
}