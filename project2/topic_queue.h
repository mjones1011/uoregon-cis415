#ifndef TOPIC_QUEUE_H
#define TOPIC_QUEUE_H

//********************
//STRUCTS
//********************
typedef struct topic TopicEntry;
typedef struct queue_topics TopicQueue;

//********************
//FUNCTIONS
//********************
TopicEntry *MallocTopicEntry(char *url, char *caption, int numEntries);
TopicQueue *MallocTopicQueue(char *name, int id, int MAXENTRIES);
long long enqueueTopic(TopicQueue *queue, char *url, char *caption);
int dequeueTopics(struct queue_topics *topicqueue, long DELTA);
long long getEntry(TopicQueue *queue, TopicEntry **t, long long lastentry);

#endif