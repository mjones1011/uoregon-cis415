#ifndef BOUNDED_QUEUE_H
#define BOUNDED_QUEUE_H

//********************
//GLOBAL VARS
//********************
//static int QUACKSIZE;
//static int CAPTIONSIZE;
//static int MAXENTRIES;

//********************
//STRUCTS
//********************
typedef struct bounded_queue BoundedQueue;

//********************
//FUNCTIONS
//********************
//???
int RoundIDToBufferIndex(int size, long long index);

//init mem + values
BoundedQueue *BB_MallocBoundedQueue(long size);

//check not full
//place item at head index (head id == ID)
//increment head
long long BB_TryEnqueue(BoundedQueue *queue,void *item); 

//if not empty
//if id is valid

//if tail == ID
//set index to null and advance tail
int BB_TryDequeue(BoundedQueue *queue,long long id);	//goal is to increment tail ID

//return head-1 // -1 if head == tail
long long BB_GetFront(BoundedQueue *queue);

//return tail // -1 if head == tail
long long BB_GetBack(BoundedQueue *queue);

//return Head - Tail
int BB_GetCount(BoundedQueue *queue);

//if tail <= id < head
int BB_IsIdValid(BoundedQueue *queue,long long id);

//check ID against tail + head to validate. See IsIdValid
//convert to index
//	return item
void *BB_GetItem(BoundedQueue *queue,long long id);

//head - tail == size
int BB_IsFull(BoundedQueue *queue);

//head == tail
int BB_IsEmpty(BoundedQueue *queue);

//cleanup and zero
void BB_FreeBoundedQueue(BoundedQueue *queue);

#endif
